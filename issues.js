require('dotenv').config();
const fetchIssues = require("./fetcher")

async function run(query) {
  const { data, nextPage, endpoint } = await fetchIssues(query)

  console.info(data)
  console.table({ totalIssues: data.length, endpoint, nextPage })
}

if (process.argv.includes('--help') || process.argv.includes('-h')) {
  return console.info(`issues usage:
    all issues        node issues
    query issues      node issues -q <query>
    
    ex. 
    node issues -q "labels=bunq&page=1"
  `)
}

const queryIdex = process.argv.indexOf('-q')
let query = ''

if (queryIdex >= 0) {
  query = process.argv?.[queryIdex + 1]
}


run(query)