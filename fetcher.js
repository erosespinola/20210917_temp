const axios = require('axios')

function fetchIssues(query) {
  const endpoint = `https://gitlab.com/api/v4/issues?${query}`
  return new Promise((resolve, reject) => {
    axios.get(endpoint, {
      headers: {
        'PRIVATE-TOKEN': process.env.TOKEN
      }
    })
      .then((response) => { 
        resolve({ data: response.data, endpoint, nextPage: response.headers['x-next-page'] })
      })
      .catch(error => reject(error))
  })
}


module.exports = fetchIssues