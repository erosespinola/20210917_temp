# 20210917_temp

## Name
GitLab Issue Fetcher

## Description
This node.js script will query the gitlab api and show the issues associated with this gitlab project.

## Installation
Clone the repo and install the dependencies.
```
git clone https://gitlab.com/erosespinola/20210917_temp.git
cd 20210917_temp
```
```
npm install
```

## Usage
To view the issues simply run the script as follows:
```
node issues
```

To get help run:
```
node issues -h
```

## Authors and acknowledgment
@erosespinola

## License
MIT